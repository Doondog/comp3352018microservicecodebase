# Microservice-Project

This microservice project was implemented by Paul Chapman (https://github.com/paulc4/microservices-demo) and was cloned
and updated for further addition of different microservice, and the Oauth was developed by techprimers 
(https://github.com/TechPrimers)

As for now, many of the pages are under construction and will be released in the future however here is the method of 
running the web browser

##How to Run the web application

1 - import the project from https://bitbucket.org/Doondog/comp3352018microservicecodebase to the intellij and make sure 
    that spring framework boot and maven are up and running

2 - run the spring-security-oauth project
   i- go to microservices-demo/spring-security-oauth/spring-security-auth-server/src/main/java/com.techprimers.security.springsecurityauthserver 
   and run the SpringSecurityAuthServerApplication
   
   ii- go to microservices-demo/spring-security-oauth/spring-security-client/src/main/java/com.techprimers.security.springsecurityclient and
   run the SpringSecurityClientApplication
 
3 - run the microservices to run the websites individually
   i- go to microservices-demo/src/java/services/accounts and run the AccountsServer java application
   
   ii- go to microservices-demo/src/java/services/accounts1 and run the AccountsServer1 java application
   
   iii- go to microservices-demo/src/java/services/accounts2 and run the AccountsServer2 java application
   
   iv- go to microservices-demo/src/java/services/home and run the HomeServer java application
   
4 - Now go to your web browser and type http://localhost:8082/ui to access the page of the Oauth login page

5 - insert username 'Kumail' and password 'kumail' or username 'Donal' and password 'donal' to access the next page which
    will require you to click on the hyperlink to access the website

6 - finally you should be able to access the website of the rental store. 
   