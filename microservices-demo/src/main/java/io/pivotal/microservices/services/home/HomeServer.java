package io.pivotal.microservices.services.home;

import java.util.logging.Logger;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Import;

import io.pivotal.microservices.home.ProductRepository;
import io.pivotal.microservices.home.ProductConfiguration;


/**
 * Run as a micro-service, registering with the Discovery Server (Eureka).
 * <p>
 * Note that the configuration for this application is imported from
 * {@link ProductConfiguration}. This is a deliberate separation of concerns.
 *
 * @author Comp335 Team 12
 */
@EnableAutoConfiguration
@EnableDiscoveryClient
@Import(ProductConfiguration.class)
public class HomeServer {

	@Autowired
	protected ProductRepository productRepository;

	protected Logger logger = Logger.getLogger(HomeServer.class.getName());

	/**
	 * Run the application using Spring Boot and an embedded servlet engine.
	 *
	 * @param args
	 *            Program arguments - ignored.
	 */
	public static void main(String[] args) {
		// Tell server to look for accounts-server.properties or
		// home-server.yml
		System.setProperty("spring.config.name", "home-server");

		SpringApplication.run(HomeServer.class, args);
	}
}
