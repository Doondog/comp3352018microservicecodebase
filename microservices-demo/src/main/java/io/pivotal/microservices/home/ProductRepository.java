package io.pivotal.microservices.home;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;

import java.util.List;
import java.util.Map;

/**
 * Repository for Product data implemented using Spring Data JPA.
 * 
 * @author Comp335 Team 12
 */
public interface ProductRepository extends Repository<Product, Long> {
	/**
	 * Find an account with the specified account number.
	 *
	 * @param productId.
	 * @return The account if found, null otherwise.
	 */
	public Product findByProductId(String productId);


	public List<Map<String, Object>> findAll();

	/**
	 * Fetch the number of accounts known to the system.
	 * 
	 * @return The number of accounts.
	 */
	@Query("SELECT count(*) from Product")
	public int countProducts();
}
