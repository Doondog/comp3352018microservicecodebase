package io.pivotal.microservices.home;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Home page controller.
 * 
 * @author Comp335 Team 12 @template-author Paul Chapman
 */
@Controller
public class HomeController {
	
	@RequestMapping("/")
	public String home() {
		return "index";
	}
	@RequestMapping("/index.html")
	public String home1() {
		return "index";
	}
	@RequestMapping("/list.html")
	public String list() {
		return "list";
	}
	@RequestMapping("/checkout.html")
	public String checkout() {return "checkout";}
	@RequestMapping("/order.html")
	public String order() {return "order";}
	@RequestMapping("/aboutus.html")
	public String aboutus() {return "aboutus";}
	@RequestMapping("/contactus.html")
	public String contactus() {return "contactus";}
	@RequestMapping("/view")
	public String view() {return "view";}
	@RequestMapping("/new.html")
	public String orderP() {return "new";}


}
