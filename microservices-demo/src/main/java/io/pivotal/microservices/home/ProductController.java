package io.pivotal.microservices.home;

import io.pivotal.microservices.exceptions.ProductNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import org.springframework.ui.Model;

import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

/**
 * A RESTFul controller for accessing account information.
 *
 * @author Comp335 Team 12
 */
@Controller
public class ProductController {

	protected Logger logger = Logger.getLogger(ProductController.class
			.getName());
	protected ProductRepository productRepository;

	/**
	 * Create an instance plugging in the respository of Accounts.
	 *
	 * @param productRepository
	 *            An account repository implementation.
	 */
	@Autowired
	public ProductController(ProductRepository productRepository) {
		this.productRepository = productRepository;

		logger.info("ProductRepository says system has "
				+ productRepository.countProducts() + " products");
	}



	/**
	 * Fetch accounts with the specified name. A partial case-insensitive match
	 * is supported. So <code>http://.../products</code> will find any
	 * accounts with upper or lower case 'a' in their name.
	 *
	 * @return A non-null, non-empty set of accounts.
	 * @throws ProductNotFoundException
	 *             If there are no matches at all.
	 */
	@RequestMapping("/home/view")
	public String findAllProducts(Model model) {
		logger.info("products-service findAll() invoked: "
				+ productRepository.getClass().getName());

		List<Map<String, Object>> products = productRepository
				.findAll();
		logger.info("products-service byOwner() found: " + products);

		int size = productRepository.countProducts();


		model.addAttribute("products", products);
		model.addAttribute("count", size);
		return "view";
	}
}