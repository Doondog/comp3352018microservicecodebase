drop table T_PRODUCT if exists;

create table T_PRODUCT (ID bigint identity primary key, ProductID varchar(20), Product_Name varchar(60),
                        Category varchar(45) not null, Description varchar(300), Image varchar(45), unique(ProductID));

