-- MySQL Script generated by MySQL Workbench
-- Sun May 20 23:22:47 2018
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`Main`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Main` (
  `ReviewID` INT NOT NULL,
  `ProductID` INT NOT NULL,
  `Review-Content` MULTILINESTRING NOT NULL,
  `Suggested-Products` MULTILINESTRING NOT NULL,
  PRIMARY KEY (`ReviewID`),
  UNIQUE INDEX `ReviewID_UNIQUE` (`ReviewID` ASC))
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
